#include <stdio.h>
#include <stdlib.h>

/* Implemente a fun��o strcmp de string.h utilizando qualquer estrutura de repeti��o.
A fun��o strcmp compara os conte�dos de duas strings e verifica se s�o iguais retornando 0. */
// https://www.ascii-code.com/ (refer�ncia para compara��es de caracteres)

int main() {
	
	char name1[100], name2[100];
	int result;
	
	printf("Type the first name:\n");
	gets(name1);
	printf("Type the second name:\n");
	gets(name2);
	
	int index = -1;
	do {
		
		index++;
		result = name1[index] - name2[index];
			
	}	while ( result == 0 && name1[index] != '\0' && name2[index] != '\0');
	
	if ( result < 0 ) {
		
		printf("%s < %s", name1, name2);
		
	} else if ( result > 0 ) {
		
		printf("%s > %s", name1, name2);
		
	} else {
		
		printf("%s = %s", name1, name2);
	}
	return 0;
}
