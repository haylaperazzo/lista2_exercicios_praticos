#include <stdio.h>
#include <string.h>

/* Implemente a função strlen de string.h utilizando qualquer estrutura de repetição.
A função strlen retorna a quantidade de caracteres em uma string. 0. */

int main() {

    char string[100];
    int i;
    
    printf("Type a word:\n");
	gets(string);
	for (i = 0; string[i] != '\0'; i++);
    printf("Length of |%s| is |%d|\n", string, i);
    return 0;
}
