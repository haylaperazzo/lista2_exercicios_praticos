#include <stdio.h>
#include <stdlib.h>

/* Implemente a fun��o strcpy de string.h utilizando qualquer estrutura de repeti��o. 
A fun��o strcpy copia o conte�do de uma string para outra. */

int main() {
	
	char name[100], name2[100];	
	int i;	
	printf("Type a name:\n");
	gets(name);
	for(i = 0; name[i] != '\0'; i++) {
		name2[i] = name[i];
	}
	printf("Name: %s \nCopy: %s", name, name2 );
	return 0;
}
