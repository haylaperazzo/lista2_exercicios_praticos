#include <stdio.h>
#include<string.h>
/* Pal�ndromo � a frase ou palavra que se pode ler, indeferentemente, da esquerda
para direita, ou vice-versa. ARARA, ANA, OVO... */
int main() {

 	int i, value;
 	char word[100], contrario[100];

	printf("\nType a word: ");
 	gets(word);

 	for(i = 0; word[i]; i++){
   	word[i] = tolower(word[i]);
 	}

 	strcpy(contrario, word);

 	strrev(contrario);

 	value = strcmp(word, contrario);

 	if (value == 0)
   	printf("\nPalindroma!!!\n", word);
	 else
   	printf("\nFalse Palindroma!!!\n", word);
 	return 0;
	}
